package br.com.aritmetica.DTOs;

import org.omg.CORBA.INTERNAL;

import java.util.List;

public class EntradaDTO {
    private List<Integer> numeros;

    public EntradaDTO() {}

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
