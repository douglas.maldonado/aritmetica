package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        if (entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie no minimo 2 numeros");
        }

        for(int n: entradaDTO.getNumeros()){
            if (n <= 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie numeros naturais");
            }
        }

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO){
        if (entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie no minimo 2 numeros");
        }

        for(int n: entradaDTO.getNumeros()){
            if (n <= 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie numeros naturais");
            }
        }

        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO){
        if (entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie no minimo 2 numeros");
        }

        for(int n: entradaDTO.getNumeros()){
            if (n <= 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie numeros naturais");
            }
        }

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO){
        int numeroAnterior = 0;

        if (entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie no minimo 2 numeros");
        }

        for(int n: entradaDTO.getNumeros()){
            if (n <= 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie numeros naturais");
            }

            if (numeroAnterior > n){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Proximo numero sempre deve ser maior");
            }

            numeroAnterior = n;
        }

        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }


}


