package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {


    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;

        for(int n: entradaDTO.getNumeros()){
            numero += n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);

        for (int n = 1; n < entradaDTO.getNumeros().size(); n++){
            numero -= entradaDTO.getNumeros().get(n);
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);

        for (int n = 1; n < entradaDTO.getNumeros().size(); n++){
            numero *= entradaDTO.getNumeros().get(n);
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);

        for (int n = 1; n < entradaDTO.getNumeros().size(); n++){
            numero = entradaDTO.getNumeros().get(n) / numero;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);

        return resposta;
    }
}
